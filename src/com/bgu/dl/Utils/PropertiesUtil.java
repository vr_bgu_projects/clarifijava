package com.bgu.dl.Utils;

import com.bgu.dl.Main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by home on 11/12/2015.
 */
public class PropertiesUtil {

    private static Properties prop = null;

    public static synchronized String readPropety(String property)
    {
            InputStream input = null;
            try {
                if (prop == null) {
                    String filename = "config.properties";
                    input = Main.class.getClassLoader().getResourceAsStream(filename);
                    if (input == null) {
                        System.out.println("Sorry, unable to find " + filename);
                        return null;
                    }
                    //load a properties file from class path, inside static method
                    prop = new Properties();
                    prop.load(input);
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                if (input != null) {
                    try {
                        input.close();
                        //get the property value
                        return (prop.getProperty(property));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        //get the property value
        return (prop.getProperty(property));
    }

}
